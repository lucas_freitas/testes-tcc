import java.time.YearMonth;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

public class MyClass {
	public static void main(String[] args) {
		MyClass myClass = new MyClass();
		CalculoDiasUteis calculoDiasUteis = new CalculoDiasUteis();
		
		myClass.exemplo1_tradicional();
		myClass.exemplo1_funcional();

		myClass.atualizarModeradoresTradicional(myClass.obterListaUsuarios());
		myClass.atualizarModeradoresFuncional(myClass.obterListaUsuarios());
		myClass.atualizarModeradoresStream(myClass.obterListaUsuarios());
		
		calculoDiasUteis.diasUteisTradicional(YearMonth.of(2019, 11));
		calculoDiasUteis.diasUteisFuncional(YearMonth.of(2019, 11));

		for (int i = 1; i<=30; i++) {
			myClass.calculaDiasUteisNoAno(2019);
		}
		
		System.out.println("\nExecutado com sucesso!");
	}

	public void calculaDiasUteisNoAno (int ano) {
		/* iterativo	-> 47 ms
		 * ToMap		-> 51 ms
		 * FlatMap		=> 77 ms */

		CalculoDiasUteis calculoDiasUteis = new CalculoDiasUteis();
		Stream.<Runnable>of(
				() -> calculoDiasUteis.diasUteisAnualIterativo(ano),
				() -> calculoDiasUteis.diasUteisAnualComCollectToMap(ano), 
				() -> calculoDiasUteis.diasUteisAnualComFlatMap(ano))
		    .map(CalculoDiasUteis::medir)
		    .forEach(System.out::println);
	}
	
	public void exemplo1_tradicional() {
		Runnable r = new Runnable() {
			public void run() {
				DisplayFunction displayFunction = new DisplayFunction() {
					@Override
					public void mostrar() {
						System.out.println("Ol�, mundo!");
					}
				};
				displayFunction.mostrar();
				DisplayFunction displayFunction2 = new DisplayFunction() {
					@Override
					public void mostrar() {
						System.out.println("Como voc� est� hoje?");
					}
				};
				displayFunction2.mostrar();
			}
		};
		new Thread(r).start();
	}

	public void exemplo1_funcional() {
		Runnable r = () -> {
			DisplayFunction displayFunction = () ->  System.out.println("Ol�, mundo!");
			displayFunction.mostrar();
			
			DisplayFunction displayFunction2 = () -> System.out.println("Como voc� est� hoje?");
			displayFunction2.mostrar();
		};
		new Thread(r).start();
	}

	private void atualizarModeradoresTradicional(List<Usuario> usuarios) {
		Collections.sort(usuarios, new Comparator<Usuario>() {
			@Override
			public int compare(Usuario u1, Usuario u2) {
				return u1.getPontos() - u2.getPontos();
			}
		});
		Collections.reverse(usuarios);
		List<Usuario> top10 = usuarios
				.subList(0, usuarios.size() > 10 ? 10 : usuarios.size());
		for (Usuario usuario : top10) {
			usuario.setModerador(true);
		}
	}
	
	private void atualizarModeradoresFuncional(List<Usuario> usuarios) {
		usuarios.sort(Comparator.comparing(Usuario::getPontos)
				.reversed());
		usuarios.subList(0, usuarios.size() > 10 
					? 10 : usuarios.size())
				.forEach(u -> u.setModerador(true));
	}
	
	private void atualizarModeradoresStream(List<Usuario> usuarios) {
		usuarios.stream()
			.sorted(Comparator.comparing(Usuario::getPontos)
					.reversed())
			.limit(10)
			.forEach(u -> u.setModerador(true));
	}
	
	private List<Usuario> obterListaUsuarios() {
		return Arrays.asList(new Usuario());
	}
	
	private List<Long> obterLista() {
		return null;
	}
	
	
	
	@SuppressWarnings("unused")
	private void exemploTradicional () {
		List<Long> lista = obterLista();
		for (Long item : lista) {
			System.out.println(item);
		}
	}
	
	@SuppressWarnings("unused")
	private void exemploLambda () {
		List<Long> lista = obterLista();
		lista.forEach(item -> System.out.println(item));
	}
	
	@SuppressWarnings("unused")
	private void exemploReferenciaDeMetodo () {
		List<Long> lista = obterLista();
		lista.forEach(System.out::println);
	}

	@SuppressWarnings("unused")
	private static Long somaIterator(List<Integer> lista) {
		return lista.stream()
				.filter(num -> num % 2 == 0)
				.count();
	}
}
