
public class Usuario {
	private String nome;
	
	private Integer idade;
	
	private Integer pontos;
	
	private Boolean moderador;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Integer getIdade() {
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public Integer getPontos() {
		return pontos;
	}

	public void setPontos(Integer pontos) {
		this.pontos = pontos;
	}

	public Boolean getModerador() {
		return moderador;
	}

	public void setModerador(Boolean moderador) {
		this.moderador = moderador;
	}
	
}
