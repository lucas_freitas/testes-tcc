import java.time.Month;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CalculoDiasUteis {
	
	public boolean fimDeSemana(LocalDate data) {
		return data.getDayOfWeek().equals(DayOfWeek.SATURDAY) 
			|| data.getDayOfWeek().equals(DayOfWeek.SUNDAY);
	}
	
	public Map<YearMonth, List<LocalDate>> diasUteisAnualIterativo(int ano) {
		Map<YearMonth, List<LocalDate>> map = new LinkedHashMap<>();
		for(Month mes : Month.values()) {
			YearMonth anoMes = YearMonth.of(ano, mes);
			List<LocalDate> diasUteisDoAno = new ArrayList<>();
			for(int dia=1; dia <= anoMes.lengthOfMonth(); dia++) {
				LocalDate data = anoMes.atDay(dia);
				if(!fimDeSemana(data)) {
					diasUteisDoAno.add(data);
				}
			}
			map.put(anoMes, diasUteisDoAno);
		}
		return map;
	}
	
	public Map<YearMonth, List<LocalDate>> diasUteisAnualComCollectToMap(int ano) {
		return Stream.of(Month.values())
			.map(month -> YearMonth.of(ano, month))
			.collect(Collectors.toMap(Function.identity(), ym -> Stream
				.iterate(ym.atDay(1),  date -> date.plusDays(1))
				.limit(ym.lengthOfMonth())
				.filter(data -> !fimDeSemana(data))
			    .collect(Collectors.toList())));
	}
	
	public Map<YearMonth, List<LocalDate>> diasUteisAnualComFlatMap(int ano) {
		return Stream.of(Month.values())
			.map(month -> YearMonth.of(ano, month))
			.flatMap(ym -> Stream
					.iterate(ym.atDay(1), date -> date.plusDays(1))
					.limit(ym.lengthOfMonth()))
			.filter(data -> !fimDeSemana(data))
			.collect(Collectors.groupingBy(date -> 
				YearMonth.of(date.getYear(), date.getMonth())));
	}
	
	public static long medir(Runnable algoritmo){
		Instant inicio = Instant.now();
		for(int i = 0; i < 1000; i++){
			algoritmo.run();
		}
		Instant fim = Instant.now();
		return Duration.between(inicio, fim).toMillis();
	}
	
	public List<LocalDate> diasUteisTradicional (YearMonth anoMes) {
		List<LocalDate> listaDosDiasUteisDoMes = new ArrayList<>();
		 
		for(int dia=1; dia <= anoMes.lengthOfMonth(); dia++){ 
		  LocalDate data = anoMes.atDay(dia); 
		  
		  if(!fimDeSemana(data)){
			  listaDosDiasUteisDoMes.add(data);
		  }
		}
		
		return listaDosDiasUteisDoMes;
	}
	
	public List<LocalDate> diasUteisFuncional (YearMonth anoMes) {
		return Stream
				.iterate(anoMes.atDay(1),  data -> data.plusDays(1))
				.limit(anoMes.lengthOfMonth())
				.filter(data -> !fimDeSemana(data))
				.collect(Collectors.toList());
	}
}
