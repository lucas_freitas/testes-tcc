package Strategy;

public class Main {
	public static void main(String[] args) {
		Validator numericValidator = new Validator (new IsNumeric());
		boolean b1 = numericValidator.validate("aaaa");
		System.out.println(b1);
		
		Validator lowerCaseValidator = new Validator(new IsAllLowerCase());
		boolean b2 = lowerCaseValidator.validate("bbbb");
		System.out.println(b2);
		
		Validator numericFunctional = new Validator((s) -> s.matches("\\d+"));
		boolean b3 = numericFunctional.validate("aaaa");
		
		ValidationStrategy lowerCaseStrategy = (s) -> s.matches("[a-z]+");
		Validator lowerCaseFunctional = new Validator(lowerCaseStrategy);
		boolean b4 = lowerCaseFunctional.validate("aaaa");
		System.out.println(b4);
	}
}
